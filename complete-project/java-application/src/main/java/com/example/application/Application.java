package com.example.application;

import com.example.gradle.JavaClass;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Hello World from the built application!");
		System.out.println("' ' is empty: " + new JavaClass().myIsEmpty(" "));
	}

}
